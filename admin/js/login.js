$(document).ready(function() {
 frmValidarLogin()
});

function frmValidarLogin() {
 jQuery("#frmLogin").validate({
  rules: {
   password: {
    required: true,
   },
   email: {
    required: true,
    email: true
   }
  },
  messages: {
   password: "Contraseña obligatoria",
   email: "Correo electrónico obligatorio ó formato incorrecto",
  },
  submitHandler: function(form) {
   datos = {}
   jQuery('#frmLogin').serializeArray().forEach(e => {
    datos[e.name] = e.value
   })

   $.ajax({
    type: "POST",
    url: "../controllers/login.php",
    data: { opc: 'ctrIngresar', datos },
    dataType: "JSON",
    success: ({ mensaje, error }) => {
     mensajes(mensaje, error)
    }
   });
  }
 });

}


let mensajes = (mensaje, error) => {
 if (!error) {
  swal({
   title: mensaje,
   icon: "success",
   button: "Ok",
  })
 } else {
  swal({
   title: mensaje,
   icon: "error",
   button: "Ok",
  })
 }
}