<?php

include_once("../../config/init_db.php");
DB::$error_handler = false;
DB::$throw_exception_on_error = true;
@session_start();

class Login
{
    public static function mdlIngresar($d)
    {
        try {
            $usuario = DB::queryFirstRow("SELECT
                                usuario_id,
                                nombrecompleto,
                                identificacion,
                                email,
                                contrasena,
                                celular,
                                direccion,
                                estado
                            FROM
                                ad_usuarios
                            WHERE
                                email = '{$d["email"]}' ");
            if (!empty($usuario)) {
                if (password_verify($d['password'], $usuario['contrasena'])) {
                    unset($usuario['contrasena']);
                    DB::query("UPDATE ad_usuarios SET fecha_ultimo_ingreso = NOW() WHERE usuario_id = '{$usuario["usuario_id"]}'");
                    $_SESSION['DistribucionesJm'] = $usuario;
                    $data['error'] = false;
                    $data['mensaje'] = 'Ingresando al sistema';
                } else {
                    $data['error'] = true;
                    $data['mensaje'] = 'Usuario ó contraseña no son validos';
                }
            } else {
                $data['error'] = true;
                $data['mensaje'] = 'Usuario ó contraseña no son validos';
            }
        } catch (MeekroDBException $e) {
            $data["error"] = false;
            $data["mensaje"] = "Usuario ó contraseña no son validos";
        }
        return $data;
    }
}