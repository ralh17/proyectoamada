<?php
date_default_timezone_set('America/Bogota');
class Datos
{

  public static

    //*************** local ***************

    $datos = [
      "host"     => "localhost",
      "user"     => "root",
      "pass"     => "",
      "db"       => "distribuciones_jm",
      "path"     => "",
      "key"      => ""
    ];

  //*************** Produccion ***************

  // $datos = [
  //   "host"     => "localhost",
  //   "user"     => "",
  //   "pass"     => "",
  //   "db"       => "distribuciones_jm",
  //   "path"     => "",
  //   "key"      => ""
  // ];
}